from django.apps import AppConfig


class RuTiketConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ru_tiket'
