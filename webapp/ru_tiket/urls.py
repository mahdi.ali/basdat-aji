from django.urls import path
from . import views

app_name = 'ru_tiket'

urlpatterns = [
    path('tiket/', views.ru_tiket, name='ru_tiket'),
    path('tiket/details/', views.ru_tiket_detail, name='ru_tiket_detail'),
    path('tiket/update/', views.ru_tiket_update_form, name='ru_tiket_update_form'),
    path('kartu-vaksin/', views.cr_kartu_vaksin, name='cr_kartu_vaksin'),
    path('kartu-vaksin/details/', views.cr_kartu_vaksin_detail, name='cr_kartu_vaksin_detail'),
    # path('ru_tiket/', views.ru_tiket, name='ru_tiket'),
]