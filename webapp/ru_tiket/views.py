from django.shortcuts import render

def ru_tiket(request):
    return render(request, 'tiket_ru.html')

def ru_tiket_detail(request):
    return render(request, 'tiket_ru_detail.html')

def ru_tiket_update_form(request):
    return render(request, 'tiket_ru_update_form.html')

def cr_kartu_vaksin(request):
    return render(request, 'cr_kartu_vaksin.html')

def cr_kartu_vaksin_detail(request):
    return render(request, 'cr_kartu_vaksin_detail.html')